package rf.androidovshchik.terminalpdf.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import timber.log.Timber;

public class ToastTrigger extends BroadcastReceiver {

	public static final String EXTRA_MESSAGE = "message";

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("ToastTrigger: received");
		if (intent.hasExtra(EXTRA_MESSAGE)) {
			Toast.makeText(context, intent.getStringExtra(EXTRA_MESSAGE), Toast.LENGTH_SHORT)
				.show();
		}
	}
}
