package rf.androidovshchik.terminalpdf.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.github.barteksc.pdfviewer.PDFView;
import com.snatik.storage.Storage;

import rf.androidovshchik.terminalpdf.R;
import rf.androidovshchik.terminalpdf.data.Preferences;
import rf.androidovshchik.terminalpdf.utils.ViewUtil;
import timber.log.Timber;

public class MainService extends BaseService {

	private WindowManager windowManager;

	private View viewer;
	private LinearLayout pinContainer;

	private Storage storage;

	private String pinCode;

	private Point point;

	private boolean ltCorner = false;
	private boolean lbCorner = false;
	private boolean rtCorner = false;
	private boolean rbCorner = false;

	@Override
	public void onCreate() {
		super.onCreate();
		windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		storage = new Storage(getApplicationContext());
		pinCode = preferences.getString(Preferences.PIN_CODE);
		point = ViewUtil.getWindow(getApplicationContext());
		startForeground(1, "Режим просмотра", R.drawable.ic_book_white_24dp);
	}

	@Override
	@SuppressWarnings("all")
	public int onStartCommand(Intent intent, int flags, int startId) {
		Timber.d("onStartCommand: Main service");
		if (!Settings.canDrawOverlays(getApplicationContext())) {
			showMessage("Нет разрешения для показа поверх других окон");
			stopWork();
			return super.onStartCommand(intent, flags, startId);
		}
		if (!storage.isFileExist(preferences.getString(Preferences.PDF_PATH))) {
			showMessage("Выбранный PDF файл не существует");
			stopWork();
			return super.onStartCommand(intent, flags, startId);
		}
        addOverlayView();
		return super.onStartCommand(intent, flags, startId);
	}

	private void addOverlayView() {
		viewer = View.inflate(getApplicationContext(), R.layout.viewer, null);
		PDFView pdfView = viewer.findViewById(R.id.pdfView);
		pinContainer = viewer.findViewById(R.id.pinContainer);
		PinLockView pinLockView = viewer.findViewById(R.id.pinLockView);
		IndicatorDots indicatorDots = viewer.findViewById(R.id.indicatorDots);

        indicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FIXED);
        pinLockView.attachIndicatorDots(indicatorDots);
        pinLockView.setPinLength(pinCode.length());
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
            	if (pin.equals(pinCode)) {
            		stopWork();
				}
			}

            @Override
            public void onEmpty() {}

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {}
        });

		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
		params.height = WindowManager.LayoutParams.MATCH_PARENT;
		params.width = WindowManager.LayoutParams.MATCH_PARENT;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
		} else {
			params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
		}
		params.flags = WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS |
			WindowManager.LayoutParams.FLAG_FULLSCREEN |
			WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
			WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
		params.format = PixelFormat.TRANSLUCENT;
		windowManager.addView(viewer, params);

		pdfView.fromFile(storage.getFile(preferences.getString(Preferences.PDF_PATH)))
			.onError((Throwable t) -> {
				Timber.e(t);
				showMessage("Ошибка при работе с PDF файлом");
				pinContainer.setVisibility(View.VISIBLE);
			})
			.onPageError((int page, Throwable t) -> {
				Timber.e(t);
				showMessage("Ошибка при отображении страницы");
				pinContainer.setVisibility(View.VISIBLE);
			})
			.onTap((MotionEvent event) -> {
				Timber.d(event.toString());
				if (event.getX() < 200 && event.getY() < 200) {
					ltCorner = true;
				} else if (event.getX() < 200 && event.getY() > point.y - 200) {
					lbCorner = true;
				} else if (event.getX() > point.x - 200 && event.getY() < 200) {
					rtCorner = true;
				} else if (event.getX() > point.x - 200 && event.getY() > point.y - 200) {
					rbCorner = true;
				} else {
					ltCorner = lbCorner = rtCorner = rbCorner = false;
				}
				if (ltCorner && lbCorner && rtCorner && rbCorner) {
					pinContainer.setVisibility(View.VISIBLE);
				}
				return false;
			})
			.load();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (viewer != null) {
			windowManager.removeView(viewer);
		}
	}
}
