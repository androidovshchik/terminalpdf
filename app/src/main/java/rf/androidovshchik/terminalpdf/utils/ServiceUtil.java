package rf.androidovshchik.terminalpdf.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class ServiceUtil {

    @SuppressWarnings("all")
    public static boolean isRunning(Context context, Class<? extends Service> serviceClass) {
        ActivityManager manager = (ActivityManager)
            context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void startServiceRightWay(Context context, Class<? extends Service> serviceClass) {
        stopServiceRightWay(context, serviceClass);
        Intent intent = new Intent(context, serviceClass);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void stopServiceRightWay(Context context, Class<? extends Service> serviceClass) {
        Intent intent = new Intent(context, serviceClass);
        if (isRunning(context, serviceClass)) {
            context.stopService(intent);
        }
    }
}
