package rf.androidovshchik.terminalpdf;

import android.app.Application;

import rf.androidovshchik.terminalpdf.data.Preferences;
import timber.log.Timber;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        Preferences preferences = new Preferences(getApplicationContext());
        preferences.printAll();
    }
}
