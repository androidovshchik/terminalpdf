package rf.androidovshchik.terminalpdf;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rf.androidovshchik.terminalpdf.data.Preferences;
import rf.androidovshchik.terminalpdf.services.MainService;
import rf.androidovshchik.terminalpdf.utils.ServiceUtil;
import timber.log.Timber;

public class MainActivity extends Activity {

    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.path)
    TextView pathView;
    @BindView(R.id.pin)
    EditText pinView;

    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        preferences = new Preferences(getApplicationContext());
        pathView.setText(getString(R.string.path, preferences.has(Preferences.PDF_PATH) ?
            preferences.getString(Preferences.PDF_PATH) : ""));
        pinView.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL |
            InputType.TYPE_NUMBER_FLAG_SIGNED);
        pinView.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        pinView.setText(preferences.getString(Preferences.PIN_CODE));
        container.requestFocus();
        if (ServiceUtil.isRunning(getApplicationContext(), MainService.class)) {
            ServiceUtil.stopServiceRightWay(getApplicationContext(), MainService.class);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!Settings.canDrawOverlays(getApplicationContext())) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 2);
        }
    }

    @OnClick(R.id.picker)
    void onPickerButton() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(new PermissionListener() {

                @Override
                public void onPermissionGranted(PermissionGrantedResponse response) {
                    new MaterialFilePicker()
                        .withActivity(MainActivity.this)
                        .withRequestCode(1)
                        .withFilter(Pattern.compile(".*\\.pdf$"))
                        .withHiddenFiles(false)
                        .start();
                }

                @Override
                public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}

                @Override
                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
            }).check();
    }

    @OnClick(R.id.start)
    void onStartButton() {
        String pinCode = pinView.getText().toString();
        preferences.putString(Preferences.PIN_CODE, pinCode);
        if (pinCode.length() < 4) {
            Toast.makeText(getApplicationContext(), "Требуется хотя бы 4 цифры для пин-кода",
                Toast.LENGTH_SHORT).show();
            return;
        }
        if (!preferences.has(Preferences.PDF_PATH)) {
            Toast.makeText(getApplicationContext(), "Не выбран PDF файл",
                Toast.LENGTH_SHORT).show();
            return;
        }
        ServiceUtil.startServiceRightWay(getApplicationContext(), MainService.class);
    }

    @Override
    public void onStop() {
        super.onStop();
        preferences.putString(Preferences.PIN_CODE, pinView.getText().toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            Timber.d("Pdf path is %s", filePath);
            preferences.putString(Preferences.PDF_PATH, filePath);
            pathView.setText(getString(R.string.path, filePath));
        } else if (requestCode == 2) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                Toast.makeText(getApplicationContext(), "Требуется разрешение для показа поверх других окон",
                    Toast.LENGTH_SHORT).show();
            }
        }
    }
}
